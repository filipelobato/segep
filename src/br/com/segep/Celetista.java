package br.com.segep;

public class Celetista extends Funcionario implements Gratificavel {	
	private static final Float gratificacao = 0.3f;
	private Float valorRecolhimentoINSS;

	public Float getGratificacao() {
		Float retorno = this.getSalario() * gratificacao;
		return retorno;
	}
	
	public Float getValorRecolhimentoINSS() { return valorRecolhimentoINSS; }
	public void setValorRecolhimentoINSS(Float valorRecolhimentoINSS) { this.valorRecolhimentoINSS = valorRecolhimentoINSS; }

}
