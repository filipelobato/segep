package br.com.segep;

public class Funcionario {
	private Integer matricula;
	private String nome;
	private String cargo;
	private Float salario;
	
	public static void main(String[] args) {
		Celetista celetista = new Celetista();
		celetista.setSalario(1000f);
		System.out.println(celetista.getGratificacao()); // Testando polimorfismo
		
		Estatuario estatuario = new Estatuario();
		estatuario.setSalario(1000f);
		System.out.println(estatuario.getGratificacao());
	}		
	
	public Integer getMatricula() {	return matricula; }
	public void setMatricula(Integer matricula) { this.matricula = matricula; }
	public String getNome() { return nome; }
	public void setNome(String nome) { this.nome = nome; }
	public String getCargo() { return cargo; }
	public void setCargo(String cargo) { this.cargo = cargo; }
	public Float getSalario() { return salario; }
	public void setSalario(Float salario) { this.salario = salario; }
}
