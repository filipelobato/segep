package br.com.segep;

public class Estatuario extends Funcionario implements Gratificavel {
	private static final Float gratificacao = 0.5f;
	private Float adicionalTempoServico;
	
	public Float getGratificacao() {
		Float retorno = this.getSalario() * gratificacao;
		return retorno;
	}
	
	public Float getAdicionalTempoServico() {return adicionalTempoServico; }
	public void setAdicionalTempoServico(Float adicionalTempoServico) { this.adicionalTempoServico = adicionalTempoServico; }

}
